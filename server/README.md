### Overview
* This is the example RMI Server.

## Usage:

* unzip rmi-server-1.0.0-SNAPSHOT.jar-bin.zip
* cd rmi-server-1.0.0-SNAPSHOT
* Run example:
<code>java -jar rmi-server-1.0.0-SNAPSHOT.jar 1099</code>

## Logging
* Edit the logback.xml file to adjust the log file location, logging level, etc. 
