package org.example.rmi.server;

import static java.lang.String.format;

import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Properties;

import org.example.rmi.ComputeEngine;

public final class Application {

    private static final String VERSION = "version";

    private Application() {
    }

    public static void main(final String[] args) throws IOException {

        if (args.length < 1) {
            final Properties properties = new Properties();
            properties.load(Application.class.getClassLoader()
                            .getResourceAsStream("META-INF/maven/org.example.rmi/rmi-server/pom.properties"));
            System.err.println(format("Usage: java -jar rmi-server-%s.jar rmi-registry-port",
                            properties.getProperty(VERSION, VERSION)));
            System.exit(1);
        }

        if (null == System.getSecurityManager()) {
            System.setSecurityManager(new SecurityManager());
        }

        final ComputeEngine computeEngine = new ComputeEngineImpl();
        final ComputeEngine computeEngineStub = (ComputeEngine) UnicastRemoteObject.exportObject(computeEngine, 0);
        final Registry registry = LocateRegistry.getRegistry(Integer.parseInt(args[0]));
        final String objectName = ComputeEngine.class.getSimpleName();
        registry.rebind(objectName, computeEngineStub);

        int count = 0;

        for (final String rmiServer : registry.list()) {

            if (0 == count) {
                System.out.println("Deployed RMI Servers");
                System.out.println("====================");
            }
            System.out.println(rmiServer);
            count++;
        }
    }
}
