package org.example.rmi.server;

import org.example.rmi.ComputeEngine;
import org.example.rmi.Task;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class ComputeEngineImpl implements ComputeEngine {

    @Override
    public <T> T executeUsing(final Task<T> task) {
        log.info("starting {}", Thread.currentThread().getStackTrace()[1].getMethodName());
        return task.execute();
    }
}
