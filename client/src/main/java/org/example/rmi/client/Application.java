package org.example.rmi.client;

import java.rmi.AccessException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import org.example.rmi.ComputeEngine;

public class Application {

    public static void main(final String[] args) throws AccessException, RemoteException, NotBoundException {

        if (args.length < 2) {
            System.err.println("Usage: java -D... -cp ... org.example.rmi.client.Application hostname rmi-registry-port");
            System.exit(1);
        }
        final Registry registry = LocateRegistry.getRegistry(args[0], Integer.parseInt(args[1]));
        final ComputeEngine computeEngineClientStub = (ComputeEngine) registry.lookup(ComputeEngine.class.getSimpleName());
        System.out.println(computeEngineClientStub.executeUsing(new PiCalculator(2)));
    }
}
