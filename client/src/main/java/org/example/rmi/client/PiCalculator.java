package org.example.rmi.client;

import java.io.Serializable;
import java.math.BigDecimal;

import org.example.rmi.Task;

public final class PiCalculator implements Task<BigDecimal>, Serializable {

    private static final long serialVersionUID = -1992187832759473696L;

    private static final BigDecimal FOUR = BigDecimal.valueOf(4);
    private static final int ROUNDING_MODE = BigDecimal.ROUND_HALF_EVEN;

    private final int digitsOfPrecisionAfterTheDecimalPoint;

    public PiCalculator(final int theNumberOfDigitsOfPrecisionAfterTheDecimalPoint) {
        digitsOfPrecisionAfterTheDecimalPoint = theNumberOfDigitsOfPrecisionAfterTheDecimalPoint;
    }

    @Override
    public BigDecimal execute() {
        final int scale = digitsOfPrecisionAfterTheDecimalPoint + 5;
        final BigDecimal arctan1u5 = arctan(5, scale);
        final BigDecimal arctan1u239 = arctan(239, scale);
        final BigDecimal pi = arctan1u5.multiply(FOUR).subtract(arctan1u239).multiply(FOUR);
        return pi.setScale(digitsOfPrecisionAfterTheDecimalPoint, BigDecimal.ROUND_HALF_UP);
    }

    private static BigDecimal arctan(final int inverseX, final int scale) {
        BigDecimal result;
        BigDecimal numer;
        BigDecimal term;
        final BigDecimal invX = BigDecimal.valueOf(inverseX);
        final BigDecimal invX2 = BigDecimal.valueOf(inverseX * inverseX);

        numer = BigDecimal.ONE.divide(invX, scale, ROUNDING_MODE);

        result = numer;
        int i = 1;

        do {
            numer = numer.divide(invX2, scale, ROUNDING_MODE);
            final int denom = 2 * i + 1;
            term = numer.divide(BigDecimal.valueOf(denom), scale, ROUNDING_MODE);
            if (i % 2 != 0) {
                result = result.subtract(term);
            } else {
                result = result.add(term);
            }
            i++;
        } while (term.compareTo(BigDecimal.ZERO) != 0);

        return result;
    }
}
