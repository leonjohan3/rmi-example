package org.example.rmi.client;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.math.BigDecimal;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.concurrent.TimeUnit;

import org.example.rmi.ComputeEngine;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class PiCalculatorIT {

    private static final String OBJECT_NAME = ComputeEngine.class.getSimpleName();
    private static final int rmiregistryPort = null == System.getProperty("rmiregistry.port") ? 0
                    : Integer.parseInt(System.getProperty("rmiregistry.port"));

    private static ComputeEngine computeEngineClientStub;

    @BeforeClass
    public static void onceBeforeRunningAllTestMethod() throws RemoteException, NotBoundException, InterruptedException {

        // give the RMI server time to complete it's startup
        TimeUnit.SECONDS.sleep(2);
        Registry registry;

        if (rmiregistryPort > 0) {
            registry = LocateRegistry.getRegistry(rmiregistryPort);
        } else {
            registry = LocateRegistry.getRegistry();
        }
        computeEngineClientStub = (ComputeEngine) registry.lookup(OBJECT_NAME);
    }

    @AfterClass
    public static void onceAfterRunningAllTestMethods() throws RemoteException, NotBoundException {

        Registry registry;

        if (rmiregistryPort > 0) {
            registry = LocateRegistry.getRegistry(rmiregistryPort);
        } else {
            registry = LocateRegistry.getRegistry();
        }

        for (final String objectName : registry.list()) {
            if (OBJECT_NAME.equals(objectName)) {
                registry.unbind(objectName);
            }
        }
    }

    @Test
    public void shouldCalculatePiWithTwoDecimalPlaces() throws RemoteException, NotBoundException {
        // given: all data (test fixture) preparation
        final PiCalculator piCalculator = new PiCalculator(2);

        // when : method to be checked invocation
        final BigDecimal result = computeEngineClientStub.executeUsing(piCalculator);

        // then : checks and assertions
        assertThat(result, is(BigDecimal.valueOf(3.14)));
    }

    @Test
    public void shouldCalculatePiWithSixDecimalPlaces() throws RemoteException, NotBoundException {
        // given: all data (test fixture) preparation
        final PiCalculator piCalculator = new PiCalculator(6);

        // when : method to be checked invocation
        final BigDecimal result = computeEngineClientStub.executeUsing(piCalculator);

        // then : checks and assertions
        assertThat(result, is(BigDecimal.valueOf(3.141593)));
    }
}
