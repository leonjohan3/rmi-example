package org.example.rmi.client;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.example.rmi.client.PiCalculator;
import org.junit.Test;

public class PiCalculatorTest {

    @Test
    public void shouldCalculatePiWithTwoDecimalPlaces() {
        // given: all data (test fixture) preparation
        final PiCalculator piCalculator = new PiCalculator(2);

        // when : method to be checked invocation
        final BigDecimal result = piCalculator.execute();

        // then : checks and assertions
        assertThat(result, is(BigDecimal.valueOf(3.14)));
    }
}
