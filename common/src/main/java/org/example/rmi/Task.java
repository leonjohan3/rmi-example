package org.example.rmi;

public interface Task<T> {
    T execute();
}
