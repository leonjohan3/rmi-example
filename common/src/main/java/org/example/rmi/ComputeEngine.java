package org.example.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ComputeEngine extends Remote {
    <T> T executeUsing(Task<T> task) throws RemoteException;
}
