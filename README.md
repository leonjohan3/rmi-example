# Note
* This file (README.md)'s markdown is based on [Markdown](https://en.wikipedia.org/wiki/Markdown).
* Run "mvn install" when running the integration tests. If not done this way, out-dated dependencies might be used.

# RMI Example
This is a [RMI](https://en.wikipedia.org/wiki/Java_remote_method_invocation) reference project. RMI, deemed old-school, might be important
for [Java Application Modernisation](https://bitbucket.org/leonjohan3/profile/wiki/Java%20Application%20Modernisation).
Additionally, because of the importance to Java Application Modernisation, this project has been configured to compile on Java SE 6.

# RMI technology
* RMI is Java specific and was included from the first version.
* Automatic stub generation for RMI objects was introduced in J2SE 5.0.

# Resources
* [Oracle Tutorial](https://docs.oracle.com/javase/tutorial/rmi/overview.html)
* [RMI Specification](https://docs.oracle.com/javase/8/docs/platform/rmi/spec/rmiTOC.html)
* [why-rmi-registry-is-ignoring-the-java-rmi-server-codebase-property](https://stackoverflow.com/questions/16769729/why-rmi-registry-is-ignoring-the-java-rmi-server-codebase-property)
* [Java RMI Properties](http://docs.oracle.com/javase/7/docs/technotes/guides/rmi/javarmiproperties.html)
* [Security Policy](https://docs.oracle.com/javase/tutorial/ext/security/policy.html)
* [RMI Enhancements in JDK 7](http://docs.oracle.com/javase/7/docs/technotes/guides/rmi/enhancements-7.html)
